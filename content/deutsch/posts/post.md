---
title: Im Nationalrat dank Social Media?
author: Balthasar Sager
date: 2020-01-12
images:
  - https://i.imgur.com/4noZOQ5.jpg
---

<b>

Wie wichtig ist eine Social Media-Kampagne für eine Wahl in den Nationalrat? Erhöhen erfolgreiche Kampagnen die Wahlchancen der Kandidierenden? Ein statistisches Modell, das den Ausgang von 98% der Kandidaturen vorherzusagen vermag, gibt Aufschluss.
</b>

<br>

![](https://i.imgur.com/VxZmxgx.png)



<br>

166'000 Mal buhlten Kandidierende in den Nationalratswahlen 2019 zwischen Anfang Januar und Ende Oktober auf Twitter und Facebook um die Gunst der Wähler. Zu Spitzenzeiten berieselten die Anwärter das Elektorat mit bis zu 1200 Tweets pro Tag.

Die Logik hinter diesem Verhalten: Wähler können in den Nationalratswahlen einzelnen Kandidaten bis zu zwei Stimmen geben oder unerwünschte Anwärter gänzlich von der Liste streichen. Persönliche Kampagnenefforts der Kandidierenden können sich somit positiv auszahlen.

Aber lohnt es sich für die Kandierenden wirklich, in diesem trotzdem eher parteienzentrierten Wahlsystem die Kräfte auf Social Media zu bündeln? Oder zählen der Listenplatz, das Geschlecht oder das zuvorige Innehaben eines Nationalratssitzes weiterhin mehr? Und gibt es bestimmte Kandidaturen, für die sich eine Social Media-Kampagne besonders auszahlte? Gibt es Kantone, in den sich eine Social Media-Kampagne eher auszahlt?

Diesen Fragen geht dieser Beitrag anhand von Twitter- und Facebook-Eckdaten 1414
Nationalratskandidierender nach, die vom [Digital Democracy Lab](https://digdemlab.io) am Institut für Politikwissenschaft an der Universität Zürich gesammelt wurden. Für diesen Beitrag wurden die Daten in einem aufwendigen Prozess vom Autor aufbereitet und erstmals mit den individuellen Wahlergebnissen der Kandidierenden verbunden.

<br>

#### Social Media als Nebenschauplatz?

Seit der Wahl von Barack Obama sind auch hiesige Kommentatoren von der Wichtigkeit des digitalen Raums für die Politik überzeugt — mit Ausnahmen: Kampagnenguru [Louis Perron bezeichnet Social Media als "Nebenschauplatz"](https://www.defacto.expert/2015/10/06/social-media-wird-ueberschaetzt/).

Wieso dürfte eine Social Media-Kampagne im Durchschnitt also unwichtig sein? 

Erstens liessen sich in den letzten Nationalratswahlen 4664 Kandidierende aufstellen. Bei einer hohen Anzahl Kandidierender sollten bei den Wählerinnen vermehrt Heuristiken wie der Listenplatz greifen — ebenfalls könnten Listen vermehrt unverändert eingeworfen werden. Kampagnen einzelner Kandidaten dürften nach dieser Logik ihre Wirkung wenig entfalten, da sich das Elektorat gar nicht erst an die Kampagnen der einzelnen Anwärter erinnern kann — jene auf den hinteren Listenplätzen werden gestrichen.

Wie mit dem Listenplatz verhält es sich auch mit dem Bisherigenstatus: Ob jemand schon zuvor in den Nationalrat abgeordnet war, dürfte ein wichtiger Entscheidungsfaktor für die Wählerschaft sein (ähnlich die Wahlforschung von Georg Lutz (2010)).

Weiter dürften bei den Wahlen (analog zu direktdemokratischen Voten) jüngere, Social Media-affine Stimmberechtigte, seltener an der Urne auftauchen als ältere Semester (Dermont 2016, Dermont und Stadelmann-Steffen 2018). Gerät eine Kampagne nicht an die Zielgruppe, kann sie auch ihre Wirkung nicht entfalten.

Ob sich Wahlberechtigte also überhaupt an die Social Media-Kampagnen einzelner Kandidierender erinnern können und sie diese dann auch wählen, ist also streitig. Wie stark hängen digitale Kampagnen also wirklich mit persönlichem Wahlerfolg zusammen? Und wenn ja, weshalb?

<br>

#### Bessere Wahlchancen durch Qualität, Quantität oder die alleinige Präsenz?

Eine Social-Media Kampagne könnte sich auf drei Arten in erhöhten Wahlchancen niederschlagen: Der Kampagnenerfolg könnte einerseits von der Quantität der Beiträge abhängen. Das Absetzen besonders vieler Social Media-Posts könnte eine mobilisierende Wirkung ausüben, indem etwa der Name einer Anwärterin oft auf dem Bildschirm eines Wählers erscheint. 

Zweitens könnte die Qualität der Kampagnenbeiträge, gemessen an der Anzahl Interaktionen der Social Media-Nutzer mit den Kampagneninhalten, wichtig sein. Sollte ein Kandidatin besonders angeregte Diskussionen in ihren Kommentarspalten, viele Weiterverbreitungen ihrer Beiträge ("Shares", "Retweets", "Quotes") oder eine Vielzahl von "Likes" verbuchen, könnte das ein Anzeichen für die Resonanz ihrer politischen Botschaften sein.

Drittens aber könnte das ledigliche Existieren einer Social Media-Kampagne, unabhängig von der Qualität oder Quantität der Beiträge, eine Signalwirkung auf die Wählerschaft ausüben. Ein solcher Indikator könnte auch versteckt messen, ob eine Kandidatur in den sozialen Medien Werbung schaltet und somit die eigene Reichweite erhöht.

<br>



#### Ein Modell des Wahlerfolgs

Um diesen Fragen nachzugehen habe ich verschiedene statistische Modelle erstellt und jenes
ausgewählt, das bei der Vorhersage der 200 Nationalratssitze die höchste Trefferquote aufweist. Das Modell kann denn auch die Wichtigkeit verschiedener Faktoren für die Wahlchancen aufschlüsseln, genauso wie es sich in verschiedenen Kantonen unterschiedlich auswirken kann.

<br>

Folgende Faktoren zieht es in Betracht:

-   Klassische Faktoren der Wahlforschung
    -   Listenplatz
    -   Geschlecht
    -   Alter
    -   Bisherigenstatus (war eine Kandidatin zuvor schon im
        Nationalrat)
-   Social Media-Faktoren
    -   Anzahl Interaktionen der Nutzer mit den Inhalten einer
        Kandidatur auf Facebook (Likes, Shares, Kommentare)
    -   Anzahl Interaktionen der Nutzer mit den Inhalten einer
        Kandidatur auf Twitter (Retweets, Favoriten)
    -   Anzahl Beiträge auf Twitter (Tweets und Retweets)
    -   Anzahl Beiträge auf Facebook
    -   Indikator, ob ein Kandidierender überhaupt eine Twitter- oder
        Facebook-Präsenz hatte und ob mindestens ein Beitrag abgesetzt
        wurde

<br>

Das beste Modell kann 137 von 200 Nationalratssitzen (oder 68.5%) anhand den oben erwähnten Hinweisen korrekt vorhersagen. Bei 91 Kandidaturen liegt es daneben — es sagt entweder fälschlicherweise eine Wahl (28) oder eine Nichtwahl (63) voraus, die restlichen 4436 Kandidaten stuft es korrekt als nicht gewählt ein.

<br>

**So korrekt vermag das beste Modell den Wahlerfolg vorhersagen:**

<table class="table table-condensed">
<thead>
<tr>
<th style="text-align:right;">
Modellvorhersage
</th>
<th style="text-align:right;">
Wahlresultat
</th>
<th style="text-align:right;">
Anzahl
</th>
<th style="text-align:right;">
Prozent der Kandidierenden
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:right;">
<span style="color: red">nicht gewählt</span>
</td>
<td style="text-align:right;">
<span style="color: red">nicht gewählt</span>
</td>
<td style="text-align:right;">
4436
</td>
<td style="text-align:right;">
95%
</td>
</tr>
<tr>
<td style="text-align:right;">
<span style="color: green">gewählt </span>
</td>
<td style="text-align:right;">
<span style="color: green">gewählt </span>
</td>
<td style="text-align:right;">
137
</td>
<td style="text-align:right;">
3%
</td>
</tr>
<tr>
<td style="text-align:right;">
<span style="color: red">nicht gewählt</span>
</td>
<td style="text-align:right;">
<span style="color: green">gewählt </span>
</td>
<td style="text-align:right;">
63
</td>
<td style="text-align:right;">
1%
</td>
</tr>
<tr>
<td style="text-align:right;">
<span style="color: green">gewählt </span>
</td>
<td style="text-align:right;">
<span style="color: red">nicht gewählt</span>
</td>
<td style="text-align:right;">
28
</td>
<td style="text-align:right;">
1%
</td>
</tr>
</tbody>
</table>


<br>

#### Social Media-Faktoren sind im Durchschnitt unwichtig

Social Media-Gurus dürften die Resultate dieser Auswertung enttäuschen: Im Durchschnitt fällt keiner der Social Media-Faktoren auf einem wissenschaftlich anerkannten Niveau statistisch signifikant aus. So sind weder erfolgreiche Social Media-Kampagnen, mit denen viele interagierten, noch solche die sich durch Quantität hervortun (eine Kandidatur produziert besonders viele Wortmeldungen) mit signifikant höheren Wahlwahrscheinlichkeiten verbunden. Vielmehr sind ein guter Listenplatz und der Bisherigen wichtig. Das Geschlecht einer Kandidatur scheint keinen signifikanten Unterschied zu machen.

Social Media-Variablen sind auch für die Vorhersagekraft des Modells vernachlässigbar: Beim Weglassen sinkt die Anzahl korrekt vorhergesagter Nationalratssitze lediglich von 68.5% auf 67% — das ist wenig.

Ein Social Media-Faktor spielt jedoch eine Rolle — wenn auch statistisch nur schwach signifikant: Nämlich ob eine Person über eine Social Media-Präsenz auf Facebook oder Twitter verfügte und mindestens einmal einen Beitrag verzeichnete. Kandidaturen, die über eine Social Media-Präsenz verfügten, verzeichneten im Durchschnitt eine 10 Prozentpunkte höhere Wahlwahrscheinlichkeit.

Das lässt einen wundern: Gab es Kandidaturen für die das Vorhandensein oder Fehlen einer Social Media-Kampagne den Ausschlag zur Wahl gab? Wie verändern sich die vorhergesagte Wahlwahrscheinlichkeiten mit dem Fehlen oder Vorhandensein der Social Media-Nutzung vom tatsächlichen Zustand, der in
den Daten beobachtet wurde, zu einem “Was-wäre-wenn”-Zustand?

<br>

#### Was wäre, wenn Kandidaten Social Media nicht benutzt hätten?

Das Fehlen einer Social Media-Kampagne könnte sich gemäss Modell sich für vier bisherige Kandidaten als fatal erwiesen zu haben. Felix Müri (SVP, ZH), Hans-Ulrich Bigler (FDP, ZH), Nicolas Rochat Fernandez (SP, VD) und Peter Schilliger (FDP, LU) verzeichneten weder auf Twitter noch Facebook Aktivitäten und wurden letzten Herbst abgewählt.

Wie die nächste Tabelle zeigt, hätten die Kandidaten allesamt Wahlwahrscheinlichkeiten über 50% verzeichnet, wenn sie Social Media in ihren Kampagnen verwendet hätten.

<br>

**Nicht wiedergewählte Bisherige, die ohne Social Media-Kampagne kandidierten — und gemäss Modell daran scheiterten:**

<table class="table table-condensed">
<thead>
<tr>
<th style="text-align:right;">
Vorname
</th>
<th style="text-align:right;">
Name
</th>
<th style="text-align:right;">
Kanton
</th>
<th style="text-align:right;">
Wahlwahrscheinlichkeit ohne Social Media
</th>
<th style="text-align:right;">
Wahlwahrscheinlichkeit mit Social Media
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:right;">
Felix
</td>
<td style="text-align:right;">
Müri
</td>
<td style="text-align:right;">
LU
</td>
<td style="text-align:right;">
43%
</td>
<td style="text-align:right;">
62%
</td>
</tr>
<tr>
<td style="text-align:right;">
Hans-Ulrich
</td>
<td style="text-align:right;">
Bigler
</td>
<td style="text-align:right;">
ZH
</td>
<td style="text-align:right;">
46%
</td>
<td style="text-align:right;">
76%
</td>
</tr>
<tr>
<td style="text-align:right;">
Nicolas
</td>
<td style="text-align:right;">
Rochat Fernandez
</td>
<td style="text-align:right;">
VD
</td>
<td style="text-align:right;">
41%
</td>
<td style="text-align:right;">
60%
</td>
</tr>
<tr>
<td style="text-align:right;">
Peter
</td>
<td style="text-align:right;">
Schilliger
</td>
<td style="text-align:right;">
LU
</td>
<td style="text-align:right;">
49%
</td>
<td style="text-align:right;">
68%
</td>
</tr>
</tbody>
</table>
<br>

Gleich wie den vier oben erwähnten Abgewählten wäre es beim Fehlen einer Social Media-Kampagne owhl den folgenden neun gewählten Nationalräten ergangen — ihre Wahlwahrscheinlichkeiten wären unter die kritische 50%-Grenze gefallen.

<br>

![](https://i.imgur.com/ulTsB6v.png)

<br>

Augenscheinig ist hier die Dominanz Bisheriger und Zürcher Nationalräte: Roger Köppel (SVP), Mauro Tuena (SVP), Hans-Ueli Vogt (SVP), Thomas Matter (SVP) und Martin Haab (SVP, wobei erst 2019 für Nathalie Rickli nachgerückt) sind allesamt Zürcher Bisherige und machten gemäss Modell allesamt grosse Sprünge dank ihren Social Media-Kampagnen. 

Nur Manuela Weichelt-Picard (Grüne, Zug) schreibt das Modell ihre Wahl ihrer Social-Media Kampagne zu — sie ist die einzige Neue, bei der Social Media gemäss Berechnungen ausschlaggebend war.

Wirken sich Social Media-Kampagnen also je nach Kanton und Bisherigenstatus anders aus?

<br>

#### Social Media-Kampagnen für Bisherige im Kanton Zürich und im Tessin top — im Graubünden ein Flop

Das oben beobachtete Muster folgt einem grösseren Trend: Fehlt die Social Media-Präsenz bei Bisherigen, verzeichnen diese im Kanton Zürich durchschnittlich 30.7 Prozentpunkte tiefere Wahlwahrscheinlichkeiten. Übertroffen wird dieser Wert nur noch vom Tessin: Dort sinken die Wahlchancen beim Fehlen um 33.4 Prozentpunkte. Im Kanton St. Gallen werden Bisherige mit einem Verlust von 16 Prozentpunken Wahlwahrscheinlichkeit abgestraft, im Aargau mit 14 und im Kanton Graubünden mit 6.

Für neue Anwärter ist der Nutzen einer Social Media-Kampagne generell beschränkt: Um maximal vier Prozentpunkte erhöhte sich die Wahlwahrscheinlichkeit für Kandidierende im Kanton Tessin, minimal um einen Prozentpunkt in den Kantonen Luzern und im Baselland.





**So viele Prozentpunkte nimmt die Wahlwahrscheinlichkeit nach Kanton und Bisherigenstatus mit Social Media-Nutzung zu:**

![](https://i.imgur.com/GJjI38Y.png)





   

#### Fazit

Social Media-Kampagnen waren in den Nationalratswahlen 2019 speziell für Bisherige mit höheren Wahlwahrscheinlichkeiten verbunden, nicht aber generell unentbehrlich. Zwar wurden auch 34 Bisherige ohne Social Media-Kampagne gewählt. Doch verschiedenste Beispiele zeigen: Schaden tun sie grundsätzlich nicht — in allen Kantonen ist die Nutzung von Social Media mit höheren Wahlchancen verbunden. Da die Wichtigkeit des digitalen Raums in Zukunft wohl eher noch zunehmen wird, sollten doch
einige Kandidierende nochmals über die Bücher.



---



### Informationen zum Blogbeitrag

- Autor: Balthasar Sager
- Seminar: Politischer Datenjournalismus, Herbstsemester 2019 
- Dozierende: Dr. Theresa Gessler, Prof. Dr. Fabrizio
  Gilardi, Alexandra Kohler
- Abgabedatum: 5. Januar 2020 (12. Januar 2020)
- Wortanzahl ohne Anhang: 1233

### Referenzen

1.  Baratloo, Alireza, et al. “Part 1: simple definition and calculation
    of accuracy, sensitivity and specificity.” (2015): 48-49.
    <a href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4614595/" class="uri">https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4614595/</a>
2.  Dermont, Clau. “Taking turns at the ballot box: selective
    participation as a new perspective on low turnout.” Swiss Political
    Science Review 22.2 (2016): 213-231.
3.  Dermont, Clau, and Isabelle Stadelmann-Steffen. “Who decides?
    Characteristics of a Vote and its Influence on the Electorate.”
    Representation 54.4 (2018): 391-413.
4.  Lutz, Georg. “First come, first served: The effect of ballot
    position on electoral success in open ballot PR elections.”
    Representation 46.2 (2010): 167-181.
    <a href="https://www.tandfonline.com/doi/full/10.1080/00344893.2010.485808" class="uri">https://www.tandfonline.com/doi/full/10.1080/00344893.2010.485808</a>
5.  Perry, Patrick O. “Fast moment‐based estimation for hierarchical
    models.” Journal of the Royal Statistical Society: Series B
    (Statistical Methodology) 79.1 (2017): 267-291.

### Daten und Methode

-   Daten
    -   Für diesen Beitrag werden Daten zur Social Media-Nutzung von
        Kandidierenden der Nationalratswahlen des [Digital Democracy Labs](https://digdemlab.io/)
        am Institut für Politikwissenschaft verwendet. Die Daten des
        Digital Democracy Labs wurden in einem aufwendigen Prozess mit
        den Wahlresultaten des Bundesamt für Statistik zusammengeführt,
        da die uneinheitlichen Namensmethodologien stark voneinander abwich. So
        mussten z.T. auch manuelle Zusammenführtabellen erstellt werden, manuell die Daten korrigiert.
    -   Die Datenanalyse erfolgte in der Open Source-Umgebung `R`.
-   Modell
    -   Als Modell wurden verschiedene hierarchische Regressionsmodelle
        (varying-slopes-and-intercepts-Multilevel-Modell mit binomialem
        Outcome) berechnet. Alle oben aufgezählten Prädiktoren variieren
        variieren nach Kanton.

### Einschränkungen dieses Beitrags

-   Kausalität
    -   Ich berechne Wahlwahrscheinlichkeiten für einen hypothetischen
        Zustand, der so nicht beobachtet werden kann (Social
        Media-Nutzung bei Nichtnutzung, Nichtnutzung bei Social
        Media-Nutzung). Da die Daten in ihrer Natur nicht in einem
        Experiment oder Quasiexperiment entstanden sind, sind solche
        Berechnungen mit der starken Annahme verbunden, dass die Social
        Media-Nutzung der ausschlaggebende, relevante Faktor ist für den
        Anstieg der Wahlwahrscheinlichkeiten. Da kein vollständiges
        kausales Modell aufgestellt wird, sollten diese Zahlen mit einer
        gesunden Portion Skepsis betrachtet werden. Die
        Wahlwahrscheinlichkeiten im hypothetischen Zustand werden anhand
        Informationen von anderen durchschnittlichen Kandidaturen
        hergestellt. Letztendlich ist das Modell also lediglich
        beschreibend.
-   Datenabdeckung
    -   Facebook
        -   Facebook-Kampagnendaten wurden vom Digital Democracy Lab nur
            für 230 von 2481 Kandidaturen, die tatsächlich auch Facebook
            verwendeten, gesammelt. In diese Analyse fliessen deshalb
            nur die Eckdaten für diese ein.
        -   Das Digital Democracy Lab unterscheidet nicht zwischen
            öffentlich zugänglichen Facebook-Seiten einer Person, denen
            gefolgt werden kann und offiziellen Kampagnenseiten. Das
            bedeutet, dass diese in den Daten vermischt wurden — wo
            allerdings eine Kampagnenseite vorhanden war, wurde diese
            verwendet. Dies bedeutet, dass die Interaktionszahlen auf
            Facebook nicht immer dasselbe Konzept messen — auf
            offiziellen Kampagnenseiten werden unter Umständen andere
            Inhalte publiziert als auf persönlichen aber öffentlich
            zugänglichen Seiten — da die Summe aller Interaktionen auf
            solchen Posts verwendet wird, können somit Messfehler
            entstehen.
    -   Instagram
        -   Daten zu Instagram-Accounts fehlen, da das Digital Democracy
            Lab diese aufgrund Zugriffsbeschränkungen seitens Instagram
            nicht sammeln konnte. Kampagnendaten von rund 900
            Instagram-Accounts können deshalb nicht in die Analyse
            einfliessen.
