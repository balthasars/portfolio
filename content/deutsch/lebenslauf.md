+++
title = "Mais c'est qui, Balthasar?"
+++

<img src="https://balthasar.help/images/favicon.png"> 

Mich begeistern neue Technologien, die Machenschaften der Privatwirtschaft und des Staates.

Bis Dezember 2021 studierte ich im Master Politikwissenschaft mit Track Datenjournalismus an der Universität Zürich. Aktuell berate ich Studierende und Unternehmen bei Datenprojekten in selbständiger Tätigkeit. Ab und an arbeite ich auch datenjournalistisch.

Zuvor absolvierte ich einen Bachelor in Politikwissenschaft und Recht (Universität Zürich), arbeitete für die Data Science-Firma [cynkra](https://www.cynkra.com) und schrieb für watson.ch Artikel.

## Stärken

- Data Science mit R, Git und Python
- Grosser Wissensdurst
- Hartnäckigkeit

## Interessen

- NLP und Machine Learning
- Lobbyismus
- Wettbewerbsrecht