+++
title = "Cool Stuff I Made"
+++

Unten finden Sie eine Liste journalistischer Arbeiten, die ich beruflich oder fürs Studium erstellt habe.

### [So klimabewusst legen Schweizer Banken an, Republik, 18.04.2022](https://www.republik.ch/2022/04/18/so-klimabewusst-legen-schweizer-banken-an)

![x](https://i.imgur.com/K0K7uih.jpg)

- Exklusive Enthüllung von signifikanten Investitionen in kohlenstoffintensive Firmen
- Analysiert 700 Milliarden Franken Aktienanlagen von Credit Suisse, Lombard Odier, Pictet, UBS, Zürcher Kantonalbank
- Herausforderungen: Recherchen zu nachhaltiger Finanzwirtschaft, Methodologie, Datenqualität
- Entwicklung R-Package [`{tidysec}`](https://github.com/balthasars/tidysec)
- Artikel: [siehe hier](https://www.republik.ch/2022/04/18/so-klimabewusst-legen-schweizer-banken-an)
- Code: [Gitlab](https://gitlab.com/balthasars/wie-klimabewusst-investieren-schweizer-banken), siehe auch zugehöriges [Workbook](https://balthasars.gitlab.io/fossile-investitionen-geschaeftsbanken-serve/index.html).

<br>

### [Beleidigende YouTube-Kommentare zu Weltwoche Daily, 22.06.2021](https://pwiweb.uzh.ch/wordpress/blog/2021/08/30/in-den-kommentaren-zu-roger-koeppels-youtube-sendung-finden-sich-strafbare-aeusserungen-und-verschwoerungstheorien-eine-uebersicht/)

- Herausforderungen: Arbeiten mit YouTube API, Hate Speech Klassifizierung mit Machine Learning, Textclustering mit Topic Modeling, Storytelling
- Entwicklung R-Package [`{wrappingtransformers}`](https://github.com/balthasars/tidysec) zum einfacheren Ansteuern von vortrainierten Transformers-Algorithmen in `R`
- Seminararbeit (Politischer Datenjournalismus, FS 2021, UZH), Note 6
- Code: [Github](https://github.com/balthasars/ddj_fs_2021_hate_speech_youtube)

<br>

### **[Beamte in der Business-Klasse, Schweiz am Wochenende, 28.12.2019](https://www.aargauerzeitung.ch/schweiz/das-echte-co2-problem-beim-bund-beamte-in-der-business-klasse-136158987)**

![x](https://i.imgur.com/Nt9miyA.jpg)

   - Datenaufbereitung und Analyse von 40'000+ Flugreisen
   - Herausforderungen: Geocoding, Data Wrangling, knappes Budget
   - Publiziert in der Schweiz am Wochenende am 28.12.2019 (Seite 2 und 3)
   - Zusammenarbeit mit Lorenz Honegger (Ex-Bundeshausredaktor CH Media)

<br>

### **[Im Nationalrat dank Social Media, unpubliziert, 12.01.2020]({{< ref "/posts/post" >}})**

   - nicht publiziert, [hier lesbar]({{< ref "/posts/post" >}}). 
   - Herausforderungen: kapriziöse externe Datenlieferung, Modellieren, Visualisieren
   - Arbeit im Seminar Politischer Datenjournalismus (Masterstudium, Universität Zürich)
   - Analyse von Kampagneneckdaten aller Nationalratskandidierender
   - Korrekte Vorhersage von 2/3 der gewählten Nationalratssitze und 98% Kandidaturen mit eigenem Modell

![](https://i.imgur.com/ulTsB6v.png)

![](https://i.imgur.com/VxZmxgx.png)

![](https://i.imgur.com/GJjI38Y.png)

<br>

### Die schmutzigen SNB-Dividenden, Work in Progress

![](https://i.imgur.com/YjCewon.jpg)

![](https://i.imgur.com/fnHDA19.jpg)

- Work in Progress (2020)
- Ausleuchten des Schweizer Nationalbank-Aktienportfolios bez. ihrer erhaltenen Dividendenausschüttungen fossiler Unternehmen
- Herausforderungen: Datenabklärungen, Methodologie, Visualisierung
- Schätzungen der erhaltenen Dividenden mit gaussschem Prozess

<br>

### [Freier Mitarbeiter für watson.ch, 01.09.2016 - 01.11.2017](https://www.watson.ch/u/search?q=Balthasar%20Sager%20)

- Erstellung von Listicles und anderen Artikeln 
- Herausforderungen: lustige und kreative Texte schreiben, Beiträge multimedial anreichern
- 09 / 2016 bis 11 / 2017 