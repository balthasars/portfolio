+++
title = "Cool Stuff I Made"
+++

A selection of data-driven articles created as a freelancer and for course work at university. 
Data products created for my clients are not displayed here.

### [Exposing Major Swiss Banks' Carbon-Intensive US Equities, Republik, 18.04.2022](https://www.republik.ch/2022/04/18/so-klimabewusst-legen-schweizer-banken-an)

![x](https://i.imgur.com/K0K7uih.jpg)

- Analyzed major Swiss banks' US equity portfolios worth > 700bn CHF using SEC filings
- Exposed significant amounts of carbon-intensive equity (original finding) between Q2/2014 and Q2/2021
- Developed open source R package [`{tidysec}`](https://github.com/balthasars/tidysec) to download 13F filings
- Challenges: Due diligence on data quality, contract negotiations, storytelling
- Article: [can be found here](https://www.republik.ch/2022/04/18/so-klimabewusst-legen-schweizer-banken-an)
- Code: [Gitlab](https://gitlab.com/balthasars/wie-klimabewusst-investieren-schweizer-banken), also see pertinent [workbook](https://balthasars.gitlab.io/fossile-investitionen-geschaeftsbanken-serve/index.html).

<br>

### [Patterns of Hate Speech in YouTube Comments on Right-Wing Podcast, 22.06.2021](https://pwiweb.uzh.ch/wordpress/blog/2021/08/30/in-den-kommentaren-zu-roger-koeppels-youtube-sendung-finden-sich-strafbare-aeusserungen-und-verschwoerungstheorien-eine-uebersicht/)

- Used pre-trained machine learning model and topic models to analyze patterns of hate speech dependent on podcast content
- Worked with YouTube API to retrieve 70k+ comments on political podcast
- Challenges: Working with YouTube API and developing working version of R package [tuber](https://github.com/balthasars/tuber), storytelling, text cluster using topic models
- Developed (now-deprecated) `R` package [`{wrappingtransformers}`](https://github.com/balthasars/tidysec) to more easily use Transformers algorithm
- Course work for political data journalism (Politischer Datenjournalismus, FS 2021, UZH), Note 6
- Code: [Github](https://github.com/balthasars/ddj_fs_2021_hate_speech_youtube) (excl. data on comments, due to privacy reasons)

<br>

### **[Beamte in der Business-Klasse, Schweiz am Wochenende, 28.12.2019](https://www.aargauerzeitung.ch/schweiz/das-echte-co2-problem-beim-bund-beamte-in-der-business-klasse-136158987)**

![x](https://i.imgur.com/Nt9miyA.jpg)

   - Analyzed 40,000+ airline trips of Swiss federal bureaucrats
   - Showed missing advances in carbon-conscious travel at federal political level
   - Challenges: Geocoding, data wrangling, small budget
   - Published in national newspaper Schweiz am Wochenende on Dec. 28, 2019 (cover story, pages 2 and 3).
   - Collaboration with Lorenz Honegger (ex-federal house editor CH Media).

<br>

### **[Elected Thanks to Social Media, 12.01.2020]({{< ref "/posts/post" >}})**

![](https://i.imgur.com/ulTsB6v.png)

![](https://i.imgur.com/VxZmxgx.png)

![](https://i.imgur.com/GJjI38Y.png)

   - Used Facebook and Instagram data to analyze impact of social media campaigns on electoral chances
   - Correct prediction of 2/3 of elected National Council seats and 98% candidacies
   - Challenges: capricious external data delivery, modeling, visualizations
   - Course Work for Political Data Journalism seminar (Master's program, 
   - Not published, [readable here]({{< ref "/posts/post" >}}). 


<br>

### Die schmutzigen SNB-Dividenden, Work in Progress

![](https://i.imgur.com/YjCewon.jpg)

![](https://i.imgur.com/fnHDA19.jpg)

- Work in Progress (2020)
- Ausleuchten des Schweizer Nationalbank-Aktienportfolios bez. ihrer erhaltenen Dividendenausschüttungen fossiler Unternehmen
- Herausforderungen: Datenabklärungen, Methodologie, Visualisierung
- Schätzungen der erhaltenen Dividenden mit gaussschem Prozess

<br>

### [Freier Mitarbeiter für watson.ch, 01.09.2016 - 01.11.2017](https://www.watson.ch/u/search?q=Balthasar%20Sager%20)

- Erstellung von Listicles und anderen Artikeln 
- Herausforderungen: lustige und kreative Texte schreiben, Beiträge multimedial anreichern
- 09 / 2016 bis 11 / 2017 