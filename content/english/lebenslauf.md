+++
title = "Who is Balthasar?"
+++

<img src="https://balthasar.help/images/favicon.png"> 

I'm excited by new technologies and subjects at the intersection of the private sector and the state.

Currently, I advise students and companies on data science projects in a freelance capacity. 
I also do data-driven journalism from time to time.

Previously, I completed a master's degree in political science at University of Zurich (Switzerland), worked for the data science company [cynkra](https://www.cynkra.com) and wrote articles for watson.ch.

## Strengths

- Data Science with R and Python
- Persistence
- Hungry for knowledge 

## Interests

- NLP and machine learning
- Lobbying research
- Competition law